import "./tailwind.css";
import "./App.css";

import { BrowserRouter, Route, Switch } from "react-router-dom";
import React, { Component } from "react";

import { AuthContextProvider } from "./context/AuthContext";
import DetailProperty from "./components/DetailProperty";
import Login from "./components/Login";
import Profile from "./components/Profile";
import Article from "./components/Article";
import Property from "./components/Property";
import Participant from "./components/Participant";
import ProtectedRoute from "./components/ProtectedRoute";
import newArticle from "./components/newArticle";

class App extends Component {
  render() {
    return (
      <AuthContextProvider>
        <BrowserRouter>
          <Switch>
            <Route path="/" exact component={Login} />
            {/* <ProtectedRoute path="/volunteer"component={Volunteer} /> */}

            <ProtectedRoute path="/property" exact component={Property} />
            <ProtectedRoute path="/profile" exact component={Profile} />
            <ProtectedRoute path="/article" exact component={Article} />
            <ProtectedRoute path="/participant" exact component={Participant} />
            <ProtectedRoute path="/article/new" exact component={newArticle} />
            <ProtectedRoute
              path="/property/:id"
              component={DetailProperty}
            />
          </Switch>
        </BrowserRouter>
      </AuthContextProvider>
    );
  }
}

export default App;
