import React, { Component } from "react";
import axios from "axios";

const axiosReq = axios.create();
const AuthContext = React.createContext();

axiosReq.interceptors.request.use((config) => {
  const token = localStorage.getItem("token");
  config.headers.Authorization = `Bearer ${token}`;
  return config;
});

export class AuthContextProvider extends Component {
  constructor() {
    super();
    this.state = {
      user: "",
      token: localStorage.getItem("token") || "",
      isLoggedIn: localStorage.getItem("token") === null ? false : true,
    };
  }

  // profile
  initUser = async () => {
    const response = await axiosReq.get(
      "http://178.128.222.175:3001/api/v1/auth/profile"
    );

    this.setState({ user: response.data });
    return response;
  };

  // login
  login = async (credentials) => {
    const response = await axiosReq.post(
      "http://178.128.222.175:3001/api/v1/auth/signin",
      credentials
    );

    const { token } = response.data;

    localStorage.setItem("token", token);

    this.setState({
      token,
      isLoggedIn: true,
    });

    return response;
  };

  //logout
  logout = () => {
    localStorage.removeItem("token");

    this.setState({
      isLoggedIn: false,
    });

    return console.log("Logout!");
  };

  render() {
    return (
      <AuthContext.Provider
        value={{
          login: this.login,
          logout: this.logout,
          initUser: this.initUser,
          ...this.state,
        }}
      >
        {this.props.children}
      </AuthContext.Provider>
    );
  }
}

// Higher Order Component
export const withAuth = (WrappedComponent) => {
  return class extends Component {
    render() {
      return (
        <AuthContext.Consumer>
          {(context) => <WrappedComponent {...this.props} {...context} />}
        </AuthContext.Consumer>
      );
    }
  };
};
