import React from "react";
import { withAuth } from "../context/AuthContext";
import { Button } from "reactstrap";

function StatusButton(props) {
  return (
    <nav>
      {props.isLoggedIn ? (
        <Button color="primary" onClick={props.logout}>
          Logout
        </Button>
      ) : (
        <Button onClick="#">Login</Button>
      )}
    </nav>
  );
}

export default withAuth(StatusButton);
