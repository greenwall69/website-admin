import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import SideBar from "./SideBar";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faList, faArrowCircleLeft } from "@fortawesome/free-solid-svg-icons";

class newArticle extends Component {
  render() {
    return (
      <div>
        <SideBar />
        <div className="main">
          <section>
            <div>
              <NavLink to="/article" className="sidebar__nav__link">
                <FontAwesomeIcon icon={faArrowCircleLeft} />
                <span className="ml-2">Back</span>
              </NavLink>
            </div>
            <div className="w-full mt-12">
              <p className="flex items-center pb-3 text-xl">
                <FontAwesomeIcon icon={faList} />
                <span className="ml-3">Artikel Baru</span>
              </p>
              <div className="overflow-auto bg-white">
                <h2>hello</h2>
              </div>
            </div>
          </section>
        </div>
      </div>
    );
  }
}

export default newArticle