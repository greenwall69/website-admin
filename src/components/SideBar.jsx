import { faUserCircle, faLandmark, faNewspaper, faStreetView } from "@fortawesome/free-solid-svg-icons";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Nav } from "reactstrap";
import { NavLink } from "react-router-dom";
import React from "react";
import StatusButton from "./StatusButton";
import { withAuth } from "../context/AuthContext";

const SideBar = () => {
  return (
    <aside className="sidebar">
      <nav>
        <ul className="sidebar__nav">
          <li>
            <NavLink to="/profile" className="sidebar__nav__link">
              <FontAwesomeIcon icon={faUserCircle} size="2x" />
              <span className="sidebar__nav__text">Profile</span>
            </NavLink>
          </li>
          <li>
            <NavLink to="/property" className="sidebar__nav__link">
              <FontAwesomeIcon icon={faLandmark} size="2x" />
              <span className="sidebar__nav__text">Property</span>
            </NavLink>
          </li>
          <li>
            <NavLink to="/participant" className="sidebar__nav__link">
              <FontAwesomeIcon icon={faStreetView} size="2x" />
              <span className="sidebar__nav__text">Participant</span>
            </NavLink>
          </li>
          <li>
            <NavLink to="/article" className="sidebar__nav__link">
              <FontAwesomeIcon icon={faNewspaper} size="2x" />
              <span className="sidebar__nav__text">Article</span>
            </NavLink>
          </li>
          <li>
            <Nav href="#" className="sidebar__nav__link">
              <span className="sidebar__nav__text">
                <StatusButton />
              </span>
            </Nav>
          </li>
        </ul>
      </nav>
    </aside>
  );
};

export default withAuth(SideBar);
