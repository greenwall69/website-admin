import React, { Component } from "react";
import { NavLink } from "react-router-dom";

import SideBar from "./SideBar";
import axios from "axios";
import { withAuth } from "../context/AuthContext";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faList, faArrowCircleLeft } from "@fortawesome/free-solid-svg-icons";

const axiosReq = axios.create();

axiosReq.interceptors.request.use((config) => {
  const token = localStorage.getItem("token");
  config.headers.Authorization = `Bearer ${token}`;
  return config;
});

class DetailProperty extends Component {
  constructor(props) {
    super(props);
    this.showProperty(this.props.match.params.id);
    this.state = {
      property: {
        location: '',
        description: '',
        image: '',
        createdBy: '',
        verifiedBy: '',
        status: '',
      },
    };
  }

  // api

  showProperty = async (id) => {
    const res = await axiosReq.get(
      `http://178.128.222.175:3001/api/v1/properties/${id}`
    );
    this.setState({ property: res.data.property });
  };

  setStatusProperty = async (id, eventStatus) => {
    await axiosReq.put(
      `http://178.128.222.175:3001/api/v1/properties/${id}/verification`,
      { status: `${eventStatus}` }
    );
    this.showProperty(id);
  };

  render() {
    let checkStatus;
    if (this.state.property.status === "verified") {
      checkStatus = (
        <span className="relative inline-block px-3 py-1 font-semibold leading-tight text-green-900">
          <span
            aria-hidden
            className="absolute inset-0 bg-green-200 rounded-full opacity-50"
          ></span>
          <span className="relative">{this.state.property.status} by {this.state.property.verifiedBy.username}</span>
        </span>
      );
    } else if (this.state.property.status === "rejected") {
      checkStatus = (
        <span className="relative inline-block px-3 py-1 font-semibold leading-tight text-red-900">
          <span
            aria-hidden
            className="absolute inset-0 bg-red-200 rounded-full opacity-50"
          ></span>
          <span className="relative">{this.state.property.status} by {this.state.property.verifiedBy.username}</span>
        </span>
      );
    } else {
      checkStatus = (
        <div>
          <button
            onClick={() => {
              this.setStatusProperty(this.props.match.params.id, "verified");
            }}
            className="px-4 py-2 mr-3 font-bold text-white bg-green-500 rounded hover:bg-green-700"
          >
            Verifikasi
          </button>
          <button
            onClick={() => {
              this.setStatusProperty(this.props.match.params.id, "rejected");
            }}
            className="px-4 py-2 font-bold text-white bg-red-500 rounded hover:bg-red-700"
          >
            Tolak
          </button>
        </div>
      );
    }
    return (
      <div>
        <SideBar />
        <div className="main">
          <section>
            <div>
              <NavLink to="/property" className="sidebar__nav__link">
                <FontAwesomeIcon icon={faArrowCircleLeft} />
                <span className="ml-2">Back</span>
              </NavLink>
            </div>
            <div className="w-full mt-12">
              <div className="flex">
                <div className="inline-block mr-auto">
                  <p className="flex items-center pb-3 text-xl">
                    <FontAwesomeIcon icon={faList} />
                    <span className="ml-3">Detail Property</span>
                  </p>
                </div>
                <div className="inline-block ml-auto">{checkStatus}</div>
              </div>

              <div className="overflow-auto bg-white">
                <table className="table-auto">
                  <tbody>
                    <tr>
                      <th className="pr-2 border">Created By</th>
                      <td className="pr-2 border">:</td>
                      <td className="pr-2 border">
                        {this.state.property.createdBy.username}
                      </td>
                    </tr>
                    <tr>
                      <th className="pr-2 border">Nama Aksi</th>
                      <td className="pr-2 border">:</td>
                      <td className="pr-2 border">
                        {this.state.property.title}
                      </td>
                    </tr>
                    <tr>
                      <th className="pr-2 border">Lokasi</th>
                      <td className="pr-2 border">:</td>
                      <td className="pr-2 border">
                        {this.state.property.location.address},{" "}
                        {this.state.property.location.city},{" "}
                        {this.state.property.location.region}
                      </td>
                    </tr>
                    <tr>
                      <th className="pr-2 border">Deskripsi Aksi</th>
                      <td className="pr-2 border">:</td>
                      <td className="pr-2 border">
                        {this.state.property.description.text}
                      </td>
                    </tr>
                  </tbody>
                </table>
                <div>
                  <div className="flex justify-center mt-5">
                    <img
                      className="justify-center rounded-lg"
                      src={this.state.property.image}
                      alt="img-property"
                    />
                  </div>
                  {/* {this.state.property.image.map((index) => (
                    <img src={index} alt="img-property" />
                  ))} */}
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    );
  }
}

export default withAuth(DetailProperty);
