import React, { Component } from "react";
import { withAuth } from "../context/AuthContext";

import SideBar from "./SideBar";

class Profile extends Component {
  componentDidMount() {
    this.props.initUser();
  }

  render() {
    return (
      <div>
        <SideBar />
        <div className="main">
          <section>
            <div>
              <h2> Halaman Profile: {this.props.user.email}</h2>
              <hr />
            </div>
          </section>
        </div>
      </div>
    );
  }
}

export default withAuth(Profile);
