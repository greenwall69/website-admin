import { NavLink } from "react-router-dom";
import React, { Component } from "react";

import SideBar from "./SideBar";
import axios from "axios";
import { withAuth } from "../context/AuthContext";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faList } from "@fortawesome/free-solid-svg-icons";

const axiosReq = axios.create();

axiosReq.interceptors.request.use((config) => {
  const token = localStorage.getItem("token");
  config.headers.Authorization = `Bearer ${token}`;
  return config;
});

class Article extends Component {
  constructor() {
    super();
    this.getArticle();
    this.state = {
      article: [],
    };
  }

  // api

  getArticle = async () => {
    let res = await axiosReq.get("http://178.128.222.175:3001/api/v1/articles");
    this.setState({ article: res.data.article });
  };

  // end-api

  // component

  CheckStatus(props) {
    const { status } = props;

    if (status === "verified") {
      return (
        <div>
          <span className="relative inline-block px-3 py-1 font-semibold leading-tight text-green-900">
            <span
              aria-hidden
              className="absolute inset-0 bg-green-200 rounded-full opacity-50"
            ></span>
            <span className="relative">{status}</span>
          </span>
        </div>
      );
    } else if (status === "rejected") {
      return (
        <div>
          <span className="relative inline-block px-3 py-1 font-semibold leading-tight text-red-900">
            <span
              aria-hidden
              className="absolute inset-0 bg-red-200 rounded-full opacity-50"
            ></span>
            <span className="relative">{status}</span>
          </span>
        </div>
      );
    } else {
      return (
        <div>
          <span className="relative inline-block px-3 py-1 font-semibold leading-tight text-orange-900">
            <span
              aria-hidden
              className="absolute inset-0 bg-orange-200 rounded-full opacity-50"
            ></span>
            <span className="relative">{status}</span>
          </span>
        </div>
      );
    }
  }

  // end-componet

  // table

  renderTableHeader() {
    return (
      <tr>
        <th className="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
          No
        </th>
        <th className="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
          Title
        </th>
        <th className="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
          Status
        </th>
        <th className="px-5 py-3 text-xs font-semibold tracking-wider text-left text-gray-600 uppercase bg-gray-100 border-b-2 border-gray-200">
          Action
        </th>
      </tr>
    );
  }

  renderTableData() {
    return this.state.article.map((article, index) => {
      const { _id, title, status } = article;

      return (
        <tr key={_id}>
          <td className="px-5 py-2 text-sm bg-white border-b border-gray-200">
            <p className="text-gray-900 whitespace-no-wrap">{index + 1}</p>
          </td>
          <td className="px-5 py-2 text-sm bg-white border-b border-gray-200">
            <p className="text-gray-900 whitespace-no-wrap">{title}</p>
          </td>
          <td className="px-5 py-2 text-sm bg-white border-b border-gray-200">
            <this.CheckStatus status={status} />
          </td>
          <td className="px-5 py-2 text-sm bg-white border-b border-gray-200">
            <NavLink to={`/property/${_id}`}>
              <button className="px-4 py-2 font-bold text-white bg-blue-500 rounded-full hover:bg-blue-700">
                Detail
              </button>
            </NavLink>
          </td>
        </tr>
      );
    });
  }

  // end-table

  render() {
    return (
      <div>
        <SideBar />
        <div className="main">
          <section>
            <div className="w-full mt-12">
              <div className="flex">
                <div className="inline-block mr-auto">
                  <p className="flex items-center pb-3 text-xl">
                    <FontAwesomeIcon icon={faList} />
                    <span className="ml-3">Artikel</span>
                  </p>
                </div>
                <div className="inline-block ml-auto">
                  <NavLink to="/article/new">
                    <button className="px-4 py-2 font-bold text-white bg-green-500 rounded hover:bg-green-700">
                      Tambah Artikel
                    </button>
                  </NavLink>
                </div>
              </div>
              <div className="overflow-auto bg-white">
                <table className="max-w-full leading-normal">
                  <thead>{this.renderTableHeader()}</thead>
                  <tbody>{this.renderTableData()}</tbody>
                </table>
              </div>
            </div>
          </section>
        </div>
      </div>
    );
  }
}
export default withAuth(Article);
