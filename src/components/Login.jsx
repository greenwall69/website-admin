import React, { Component } from "react";
import { withAuth } from "../context/AuthContext";
import { Redirect } from "react-router-dom";
import "../App.css";

class Login extends Component {
  state = {
    email: "",
    password: "",
  };

  handleChange = (e) => {
    const { name, value } = e.target;
    this.setState({
      [name]: value,
    });
  };

  hanleSubmit = (e) => {
    e.preventDefault();
    this.props.login(this.state);
  };

  render() {
    if (this.props.isLoggedIn) return <Redirect push to="/profile" />;
    return (
      <div>
        <div className="flex justify-center mt-48">
          <div className="w-full max-w-xs">
            <h1 className="font-bold text-center">SIGN IN</h1>
            <form
              onSubmit={this.hanleSubmit}
              className="px-8 pt-6 pb-8 mb-4 bg-white rounded shadow-md"
            >
              <div className="mb-4">
                <label
                  className="block mb-2 text-sm font-bold text-gray-700"
                  // htmlFor="email"
                >
                  Email
                </label>
                <input
                  className="w-full px-3 py-2 leading-tight text-gray-700 border rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                  onChange={this.handleChange}
                  value={this.state.email}
                  type="email"
                  placeholder="Email"
                  name="email"
                />
              </div>
              <div className="mb-6">
                <label
                  className="block mb-2 text-sm font-bold text-gray-700"
                  htmlFor="password"
                >
                  Password
                </label>
                <input
                  className="w-full px-3 py-2 mb-3 leading-tight text-gray-700 border border-red-500 rounded shadow appearance-none focus:outline-none focus:shadow-outline"
                  onChange={this.handleChange}
                  value={this.state.password}
                  type="password"
                  placeholder="Password"
                  name="password"
                />
              </div>
              <div className="flex items-center justify-between">
                <button
                  className="px-4 py-2 font-bold text-white bg-blue-500 rounded hover:bg-blue-700 focus:outline-none focus:shadow-outline"
                  type="submit"
                >
                  Sign In
                </button>
              </div>
            </form>
            <p className="text-xs text-center text-gray-500">
              &copy;2020 SSembara. All rights reserved.
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default withAuth(Login);
